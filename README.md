Image classification of 10 types of food from the food101 dataset. We used InceptionResNetV2 architecture with no pre-trained weight.

Created by:
- Muhammad Hanif Fahreza
- Matthew Tumbur Parluhutan
- Fadhil Rasendriya Prabowo

The result:

![alt text](results/TrainingTestingLossAccuracy.png)

![alt text](results/ClassificationReport.png)

![alt text](results/ConfusionMatrix.png)
